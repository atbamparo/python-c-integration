#include <fstream>
#include <Python.h>
#include <vector>

static std::vector<PyObject*> callbacks;
void callall()
{
	for (auto it = callbacks.begin(); it != callbacks.end(); it++)
	{
		auto callable = *it;
		auto tuple = PyTuple_New(0);
		PyObject_CallObject(callable, tuple);
	}
}

static PyObject* python_add_callback(PyObject* self, PyObject* args)
{
	PyObject* callable;
	PyArg_ParseTuple(args, "O", &callable);
	if (!PyCallable_Check(callable))
	{
		PyErr_SetString(PyExc_ValueError, "Argument is not callable");
		return Py_None;
	}
	Py_XINCREF(callable);
	callbacks.push_back(callable);
}

static PyObject* python_sum(PyObject* self, PyObject* args)
{
	int a, b;
	PyArg_ParseTuple(args, "ii", &a, &b);
	int soma = a + b;

	callall();
	return Py_BuildValue("i", soma);
}

static PyMethodDef ExportMethods[] = {
	{"sum", python_sum, METH_VARARGS, "CHAMA A FUNCAO EM C"},
	{"add_callback", python_add_callback, METH_VARARGS, "adiciona callback"},
	{NULL, NULL, 0, NULL} /* Sentinel */
};
static PyModuleDef luatestmodule = {
	PyModuleDef_HEAD_INIT,
	"luatest",
	NULL,
	-1,
	ExportMethods
};
PyMODINIT_FUNC PyInit_luatestmodule(void)
{
	return PyModule_Create(&luatestmodule);
}

int main(int argc, char *argv[])
{
	PyImport_AppendInittab("luatest", PyInit_luatestmodule);
	Py_Initialize();

	FILE *script;
	fopen_s(&script, "python\\script.py", "r");
	PyRun_SimpleFileEx(script, "python\\script.py", true);
	PyRun_SimpleString("main()");

	Py_Finalize();
	
	return 0;
}

