Create virtualenv directory "env" (py -m virtualenv env), activate it (env/Scripts/activate) and install the requirements from requirements.txt (pip install -r requirements.txt).
