import os


here = os.path.dirname(__file__)
print("here: {}".format(here))
activate_this_script = os.path.join(here, 'env', 'Scripts', 'activate_this.py')
with open(activate_this_script) as file:
    exec(file.read(), dict(__file__=activate_this_script))

import luatest
from flask import Flask
app = Flask(__name__)


def death_from_above():
    print("DEATH WILL FALL FROM ABOVE!!!")


@app.route('/<int:v1>/<int:v2>')
def hello_world(v1, v2):
    r = luatest.sum(v1, v2)
    return 'Hello World! {}'.format(r)

def main():
    luatest.add_callback(death_from_above)
    app.run()
